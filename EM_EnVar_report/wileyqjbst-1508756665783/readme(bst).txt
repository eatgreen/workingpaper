readme(bst).txt for wileyqj.bst
Version 1.00 released 21 September 2007

*** IMPORTANT NOTE ***
When using the wileyqj.bst file, you will need to add
the following lines at the top of your TeX file:

\usepackage{natbib}
\setlength{\bibsep}{1pt}
\newcommand{\bibfont}{\footnotesize}
