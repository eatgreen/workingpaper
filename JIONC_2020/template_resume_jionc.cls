\NeedsTeXFormat{LaTeX2e}[1997/06/01]
\ProvidesClass{template_resume_jionc}[2014/04/11]

%
% Parent class: article.
%
\newcommand\@P@rentCl@ss{article}

%
% Define options: pass anything to parent class except some options
% that should not be used.
%
%\tracingmacros=1
\newcommand\@Reject@ption[1]{%
  \DeclareOption{#1}{\@latex@warning{Option `#1' not used}}}
\@Reject@ption{a5paper}
\@Reject@ption{b5paper}
\@Reject@ption{letterpaper}
\@Reject@ption{legalpaper}
\@Reject@ption{executivepaper}
\@Reject@ption{landscape}
\@Reject@ption{10pt}
\@Reject@ption{12pt}
\@Reject@ption{twoside}
\@Reject@ption{twocolumn}
%
% Conversely, declare some options, if only to pass them on.
%
\newcommand\@Pass@ption[1]{%
  \DeclareOption{#1}{\PassOptionsToClass{\CurrentOption}{\@P@rentCl@ss}}}
\@Pass@ption{a4paper}
\@Pass@ption{11pt}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{\@P@rentCl@ss}}

%
% Default options: A4 paper, 11pt, etc.
%
\ExecuteOptions{a4paper,11pt}
\ProcessOptions

%
% Load packages.
%

% AMS math extensions.
\RequirePackage{amsmath,amssymb}%Disponibles � http://www.ams.org/tex/amslatex.html

% Font selection: Times/Helvetica/Courier family.
%\RequirePackage[T1]{fontenc}
%\RequirePackage{txfonts}
%\RequirePackage[scaled=.90]{helvet}

% Babel language extensions: French.
\RequirePackage[frenchb]{babel} % frenchb is NOT a typo.  Do not use "french". Disponible � : http://www.ctan.org/tex-archive/macros/latex/required/babel/

%
% Load parent class.
%
\LoadClass{\@P@rentCl@ss}

%
% Define page geometry to fit the Word template
\setlength{\voffset}{-16.5mm}%Je m'attendais � 7.1mm
\setlength{\hoffset}{-5.2mm}%Je m'attendais � +1.6mm
\setlength{\paperheight}{297mm}
\setlength{\paperwidth}{210mm}
\setlength{\topmargin}{0mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\textheight}{240mm}
\setlength{\textwidth}{170mm}%Je m'attendais � 156mm
\setlength{\headheight}{13.6mm}
\setlength{\marginparsep}{0mm}
\setlength{\headsep}{10mm}
\setlength{\marginparwidth}{0mm}
\setlength{\footskip}{0mm}
\setlength{\marginparpush}{0mm}
%
\setlength{\parindent}{10mm}
\setlength{\parskip}{0pt \@plus 3pt}
\renewcommand{\baselinestretch}{0.97}   % Same as Word, squeeze it together.


%Set font size to force title font size and others I can not control otherwise !
\newcommand{\titlefontsize}{\fontsize{14pt}{14pt}\selectfont}%{\titlefontsize Text is written at 14pt}
\newcommand{\sectionfontsize}{\fontsize{11pt}{11pt}\selectfont}%{\normalfontsize Text is written at 11pt}


%
% Page style: empty.
\pagestyle{empty}

%
% Title:
% - add \thispagestyle{empty};
% - geometry;
% - add macros \email, \abstract, ;
% - special macro \institution.
%
\let\@ldmaketitle\maketitle
\def\maketitle{%
  \@ldmaketitle
  \thispagestyle{empty}%
  \global\let\@email\@empty
  \global\let\@institution\@empty
  \global\let\@abstract\@empty
  \global\let\email\relax
  \global\let\institution\relax
  \global\let\abstract\relax
}
\def\@maketitle{%
  \newpage
  \let \footnote \thanks
  \begin{center}%
    {\fontfamily{ptm} \titlefontsize \bfseries  \@title \par}%
    \vskip 21pt%
    {\normalsize  \@author \par}%
    \vskip 6pt%
    {\emph \@institution}
    \par
    \@email
  \end{center}
  \begin{quotation}%
  \raggedright
    \noindent
    \end{quotation}
}
\def\email#1{\gdef\@email{\vskip 6pt{\small {#1}\par}}}
\gdef\@email{}


%
% Institution: framework for authors with multiple affiliations.
\def\institution{%
  \@ifnextchar[{\@new@institution}{\@new@institution@dfl}%]
}
\def\@new@institution[#1]#2{%
  \global\@namedef{@institution@#1}{\instref{#1}~#2\par}%
  \addto\@institution{\csname @institution@#1\endcsname}
}
\def\@new@institution@dfl#1{%
  \global\@namedef{@institution@}{#1\par}%
  \addto\@institution{\csname @institution@\endcsname}
}
\gdef\@institution{}
\def\instref#1{\ensuremath{{}^{#1}}}

%
% Sections: small caps, centered.
\renewcommand\thesection{\arabic{section}.}
\renewcommand\section{\@startsection {section}{1}{\z@}%
                     {-9pt \@plus -3pt \@minus -1pt}%
                     {6pt}%
                     {\centering\sectionfontsize\bfseries\scshape}}
\renewcommand\thesubsection{\arabic{section}.\arabic{subsection}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                       {-3.25ex\@plus -1ex \@minus -.2ex}%
                                       {1.5ex \@plus .2ex}%
                                       {\normalfont\normalsize\bfseries}}


\renewcommand{\figurename}{Fig.}
\renewcommand{\tablename}{Tab.}




%From http://dcwww.camd.dtu.dk/~schiotz/comp/LatexTips/LatexTips.html
% Different font in captions
\newcommand{\captionfonts}{\small}
\makeatletter  % Allow the use of @ in command names
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \sbox\@tempboxa{{\captionfonts #1 : #2}}%
  \ifdim \wd\@tempboxa >\hsize
    {\captionfonts #1 : #2\par}
  \else
    \hbox to\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}
\makeatother   % Cancel the effect of \makeatletter



%Bibliography in \small 
\renewenvironment{thebibliography}[1]
 { %\ninerm
   \small\rm
   \begin{list}{[\arabic{enumi}]}
    {\usecounter{enumi} \setlength{\parsep}{0pt}
    \setlength{\topsep}{0pt}
     \setlength{\itemsep}{3pt} \settowidth{\labelwidth}{#1.}
   \sloppy
   }}{\end{list}}

